# Welsh Language Statistics

If you want to know more about this, [read this](README.md).

Here are the apps:

- https://f-droid.org/cy/packages/com.gitlab.ardash.appleflinger.android
- https://f-droid.org/cy/packages/org.commonvoice.saverio
- https://f-droid.org/cy/packages/org.fdroid.fdroid
- https://f-droid.org/cy/packages/fr.gouv.etalab.mastodon
- https://f-droid.org/cy/packages/org.jellyfin.mobile
- https://f-droid.org/cy/packages/org.projectmaxs.module.alarmset
- https://f-droid.org/cy/packages/org.projectmaxs.module.bluetooth
- https://f-droid.org/cy/packages/org.projectmaxs.module.bluetoothadmin
- https://f-droid.org/cy/packages/org.projectmaxs.module.clipboard
- https://f-droid.org/cy/packages/org.projectmaxs.module.contactsread
- https://f-droid.org/cy/packages/org.projectmaxs.module.fileread
- https://f-droid.org/cy/packages/org.projectmaxs.module.filewrite
- https://f-droid.org/cy/packages/org.projectmaxs.module.locationfine
- https://f-droid.org/cy/packages/org.projectmaxs.module.misc
- https://f-droid.org/cy/packages/org.projectmaxs.module.nfc
- https://f-droid.org/cy/packages/org.projectmaxs.module.notification
- https://f-droid.org/cy/packages/org.projectmaxs.module.phonestateread
- https://f-droid.org/cy/packages/org.projectmaxs.module.ringermode
- https://f-droid.org/cy/packages/org.projectmaxs.module.shell
- https://f-droid.org/cy/packages/org.projectmaxs.module.smsnotify
- https://f-droid.org/cy/packages/org.projectmaxs.module.smsread
- https://f-droid.org/cy/packages/org.projectmaxs.module.smssend
- https://f-droid.org/cy/packages/org.projectmaxs.module.smswrite
- https://f-droid.org/cy/packages/org.projectmaxs.module.wifiaccess
- https://f-droid.org/cy/packages/org.projectmaxs.module.wifichange
- https://f-droid.org/cy/packages/com.simplemobiletools.calculator
- https://f-droid.org/cy/packages/com.simplemobiletools.camera
- https://f-droid.org/cy/packages/com.simplemobiletools.clock
- https://f-droid.org/cy/packages/com.simplemobiletools.contacts.pro
- https://f-droid.org/cy/packages/com.simplemobiletools.draw.pro
- https://f-droid.org/cy/packages/com.simplemobiletools.filemanager.pro
- https://f-droid.org/cy/packages/com.simplemobiletools.flashlight
- https://f-droid.org/cy/packages/com.simplemobiletools.notes.pro
- https://f-droid.org/cy/packages/de.westnordost.streetcomplete


## output

```
$ python3 lang.py cy
Looking for language: cy
com.gitlab.ardash.appleflinger.android
org.commonvoice.saverio
org.fdroid.fdroid
fr.gouv.etalab.mastodon
org.jellyfin.mobile
org.projectmaxs.module.alarmset
org.projectmaxs.module.bluetooth
org.projectmaxs.module.bluetoothadmin
org.projectmaxs.module.clipboard
org.projectmaxs.module.contactsread
org.projectmaxs.module.fileread
org.projectmaxs.module.filewrite
org.projectmaxs.module.locationfine
org.projectmaxs.module.misc
org.projectmaxs.module.nfc
org.projectmaxs.module.notification
org.projectmaxs.module.phonestateread
org.projectmaxs.module.ringermode
org.projectmaxs.module.shell
org.projectmaxs.module.smsnotify
org.projectmaxs.module.smsread
org.projectmaxs.module.smssend
org.projectmaxs.module.smswrite
org.projectmaxs.module.wifiaccess
org.projectmaxs.module.wifichange
com.simplemobiletools.calculator
com.simplemobiletools.camera
com.simplemobiletools.clock
com.simplemobiletools.contacts.pro
com.simplemobiletools.draw.pro
com.simplemobiletools.filemanager.pro
com.simplemobiletools.flashlight
com.simplemobiletools.notes.pro
de.westnordost.streetcomplete
34 apps in cy
```
