#/usr/bin/python3
import json
import sys

LANG = sys.argv[1].lower()

print("Looking for language: {}".format(LANG))

with open("index-v1.json", "r") as f:
    d = json.load(f)

collected = set()
found = 0
for app in d["apps"]:
    printed = False
    for lang in app.get("localized", []):
        collected.add(lang)
        if LANG in lang.lower() and not printed:
            found += 1
            printed = True
            print(app["packageName"])

if not found:
    print("Choose a language: {}".format(", ".join(sorted(collected))))
    print("lang.py <language>")
else:
    print("{} apps in {}".format(found, LANG))
