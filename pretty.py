#/usr/bin/python3
import json


with open("index-v1.json", "r") as f:
    d = json.load(f)

with open("pretty.json", "w") as f:
    json.dump(d, f, indent="  ")


