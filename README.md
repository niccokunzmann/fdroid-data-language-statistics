# Language Statistics from F-Droid

Which apps have a translation in a language?

## Usage

Install Python 3

E.g. for Welsh (cy):
```
python3 lang.py cy
```

## Source

https://f-droid.org/repo/index-v1.jar
You can download other sources from other hosts.

### Update

```
wget https://f-droid.org/repo/index-v1.jar
unzip index-v1.jar
```

### Pretty look at the index

```
python3 pretty.py
```
## References

- https://forum.f-droid.org/t/statistics-apps-with-summary-description-by-language-at-f-droid-org/18052/30
- https://toot.wales/@niccokunzmann/108208492285507807

